#!/bin/bash

# ONLY AS ENV VARS
# GH_TOKEN="$1"
# GH_REPO_O="$2"
# GH_REPO_N="$3"
# GH_REPO_B="$4"

curl \
-H "Accept: */*" \
-H "Authorization: token $GH_TOKEN" \
-L "https://api.github.com/repos/$GH_REPO_O/$GH_REPO_N/zipball/$GH_REPO_B" > imported.zip

unzip -qq imported.zip

rm imported.zip
mv "$GH_REPO_O-$GH_REPO_N-"* imported
rm imported/Dockerfile
mv imported/* ./
rmdir imported
bash run.sh
